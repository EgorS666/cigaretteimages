﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CannyEdgeDetectionCSharp {
  class U {

    /// <summary>
    /// Возвращает первое незанятое имя файла вида aaa(35).ext или пустую строку, если такое имя найти не удалось.
    /// </summary>
    /// <param name="fNameFull"></param>
    /// <returns></returns>
    public static string mkFreshName(string fNameFull) {
      if (fNameFull == "") {
        return "";
      }

      if (!File.Exists(fNameFull)) {
        return fNameFull;
      }
      
      string ext, fName, fNameBase;
      int indPeriod = fNameFull.LastIndexOf('.');
      int i = 2;
      if (indPeriod >= 0) {
        fNameBase = fNameFull.Substring(0, indPeriod);
        ext = fNameFull.Substring(indPeriod);

        
      } else {
        ext = "";
        fNameBase = fNameFull;
      }

      fName = fNameBase + "(2)";
      while (File.Exists(fName + ext) && i < 1001) {
        ++i;
        fName = fNameBase + "(" + Convert.ToString(i) + ")";
      }
      if (!File.Exists(fName + ext)) {
        return (fName + ext);
      } else {
        return "";
      }
    }
  }
}
