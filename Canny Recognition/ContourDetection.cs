﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CannyEdgeDetectionCSharp {

  /*
  class ContourDetection {
    
    public Point centerOfMass(int[,] imgMap) {
      long numeratorX = 0, denominator = 0, numeratorY = 0;
      for (int i = 0; i < imgMap.GetLength(0); ++i) {
        for (int j = 0; j < imgMap.GetLength(1); ++j) {
          numeratorX += i * imgMap[i, j];
          numeratorY += j * imgMap[i, j];
          denominator += imgMap[i, j];
        }
      }
      Point center = new Point { x = Convert.ToInt32(Convert.ToDouble(numeratorX) / Convert.ToDouble(denominator)),
                                 y = Convert.ToInt32(Convert.ToDouble(numeratorY) / Convert.ToDouble(denominator)) };
      if (center.x < 0) {
        center.x = 0;
      } else if (center.x >= imgMap.GetLength(0)) {
        center.x = imgMap.GetLength(0) - 1;
      } else if (center.y < 0) {
        center.y = 0;
      } else if (center.y >= imgMap.GetLength(1)) {
        center.y = imgMap.GetLength(1) - 1;
      }
      return center;
    }




    /// <summary>
    /// Находит минимальный (или типа того) прямоугольник с центром в центре масс, покрывающий собой нужную
    /// долю ('threshold') суммарной величины всего изображения.
    /// </summary>
    /// <param name="imgMap"></param>
    /// <param name="centerOfMass"></param>
    /// <param name="threshold">Должен быть из (0; 1.0)</param>
    /// <param name="topLeftCorner"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    public void findKeyRectangle(int[,] imgMap, Point centerOfMass, double threshold,
                                  out Point topLeftCorner, out int width, out int height) {
      long total = 0, desired, current;
      int STEP = 10, coordLeft, coordTop, coordRight, coordBottom;

      int imgHeight = imgMap.GetLength(0), imgWidth = imgMap.GetLength(1);

      for (int i = 0; i < imgMap.GetLength(0); ++i) {
        for (int j = 0; j < imgMap.GetLength(1); ++j) {
          total += imgMap[i, j];
        }
      }
      desired = Convert.ToInt64(Convert.ToDouble(total) * threshold);
    }


  }
  
  public struct Point {
    public int x;
    public int y;
  }
  */
}
